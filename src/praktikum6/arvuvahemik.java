package praktikum6;

import lib.TextIO;
public class arvuvahemik{
	
	public static int kasutajaSisestus(int min, int max){
		while (true){
			
		System.out.println("Palun sisestage arv vahemikus " + min + " kuni " + max);
		int sisestus = TextIO.getlnInt();
		
		if (sisestus >= min && sisestus <=max) {
			return sisestus;
		} else {
			System.out.println("See arv ei sobi vahemikku");
		}
	}
}
	public static void main(String[] args){
		int hinne = kasutajaSisestus(1, 5);
		
		System.out.println("Kasutaja sisestas: " + hinne);
					
	}
}
