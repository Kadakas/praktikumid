package praktikum6;

import lib.TextIO;

public class arvukuup {
		public static void main(String[] args){
			System.out.println("Sisestage arv, mida soovite kuupi võtta");
			
			int arv = TextIO.getlnInt();
			int arvKuubis = kuup(arv);
			System.out.println(arvKuubis);
		}
		private static int kuup(int number) {
			return (int) Math.pow(number, 3);
		}
		
}
