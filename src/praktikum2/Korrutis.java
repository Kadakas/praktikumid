package praktikum2;

public class Korrutis {
	public static void main(String[] args){
		//lühem variant
		//System.out.println("Sisestage kaks arvu")
		//System.out.println("Arvude korrutis on: " + TextIO.getInt() * TextIO.getInt())
		
		//defineeri muutujad
		int arv1;
		int arv2;
		int korrutis;
		
		//küsime 2 arvu 
		System.out.print("Sisesta kaks arvu: ");
		arv1 = TextIO.getInt();
		arv2 = TextIO.getInt();

		
		//arvutame korrutise
		korrutis = arv1 * arv2;
		//väljastamine
		System.out.println("Nende arvude korrutis on: " + korrutis);
		
	}
}
