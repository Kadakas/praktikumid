package praktikum2;

public class Nimepikkus {
	public static void main(String[] args) {

		System.out.println("Sisestage nimi: ");
		String nimi = TextIO.getlnString();

		int nimePikkus = nimi.length();
		System.out.println("Teie nimes on " + nimePikkus + " tähte");
	}

}
