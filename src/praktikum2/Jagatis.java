package praktikum2;

public class Jagatis {
	public static void main(String[] args) {
		// muutujate defineerimine
		// Küsime suurusid
		System.out.print("Sisestage inimeste arv ");
		int inimesed = TextIO.getInt();

		System.out.print("Sisestage grupi suurus");
		int grupisuurus = TextIO.getInt();
		// arvutus

		int jagatis = inimesed / grupisuurus;
		int leftover = inimesed % grupisuurus;
		// väljastus
		System.out.println("Moodustada saab " + jagatis + " gruppi");
		System.out.println("Üle jääb " + leftover + " inimest");
	}
}