package praktikum5;

import praktikum2.TextIO;

public class tabelX {
	public static void main(String[] args) {
		System.out.println("Sisestage tabeli suurus");
		int tabelisuurus = TextIO.getlnInt();

		for (int i = 1; i <= tabelisuurus; i++) {
			for (int j = 1; j <= tabelisuurus; j++) {
				if (i == j || i + j == tabelisuurus - 1) { //muutujad on sama väärtusega tuleb diagonaal
					System.out.print("x ");
				} else {
					System.out.print("0 ");
				}
			}
				System.out.println(); // reavahetus
			}
		}
	}

