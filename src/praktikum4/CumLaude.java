package praktikum4;

import lib.TextIO;

public class CumLaude {
	public static void main(String[] args) {
		System.out.println("Sisestage lõputöö hinne");
		int loputoo = TextIO.getlnInt();
		if (loputoo < 0 || loputoo > 5){
			System.out.print("See ei ole korrektne lõputöö hinne");
			return;
		}
		System.out.println("Sisestage oma keskmine hinne");
		double keskhinne = TextIO.getlnDouble();
		if (keskhinne < 0 || keskhinne > 5){
			System.out.print("See ei ole korrektne keskhinne hinne");
			return;}
		if (loputoo == 5 && keskhinne >= 4.5) {
			System.out.print("Saad Cum Laude!");
		} else {
			System.out.print("Ei saa Cum Laudet");
		}

	}

	}
