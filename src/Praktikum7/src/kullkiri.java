package prak7;

public class kullkiri {
	public static void main(String[] args) {
		// 0 on kull ja 1 on kiri
		int kasutajaArvab = meetod.kasutajaSisestus("Sisesta kull (0) või kiri (1)", 0, 1);
		int arv = (Math.random() > 0.5) ? 0 : 1; // lühike if lause
		int kasutajaRaha = 100;

		while (kasutajaRaha > 0) {
			System.out.println("Sul on: " + kasutajaRaha + " raha");
			int panus = meetod.kasutajaSisestus("Sisesta panus (max 25)", 1, 25);
			kasutajaRaha -= panus;

			System.out.println("Kasutaja sisestas: " + kasutajaArvab);
			if (kasutajaArvab == arv) {
				System.out.println("Arvasid ära");
				kasutajaRaha += panus * 2;
			} else {
				System.out.println("Arvasid valesti");
			}
		}
	}
}