package praktikum13;


public class Massiivid {

	public static void main(String[] args) {
		int[] arvud = {3, 4, 6, -3, 9};
		int [][] neo = {
				{1, 1, 1, 1, 1},
				{2, 3, 4, 5, 6},
				{3, 4, 5, 6, 7},
				{4, 5, 6, 7, 8},
				{5, 6, 7, 8, 9},
		};
		System.out.println(reaMaksimum);

}
	/*Kirjutada meetod, mis trükib ekraanile ühel real parameetrina 
	 *etteantud ühemõõtmelise täisarvumassiivi elemendid 
	 */
	
	public static void tryki(int[] massiiv){
		
		for (int i = 0; i < massiiv.length; i++) {
			System.out.println(massiiv[i] + " ");
			
		}
		
	}
	/*Kirjutada meetod, mis trükib ekraanile parameetrina etteantud
	 *  kahemõõtmelise massiivi (maatriksi)
	 */
	public static void tryki(int[][] maatriks){
		for (int[] rida : maatriks) {
		tryki(rida);	
		}
	}
	/*Arvutada maatriksi iga rea elementide summa
	*/
		public static int[] ridadeSummad(int[][] maatriks){
			int[] summad = new int[maatriks.length];
			for (int i = 0; i < maatriks.length; i++) {
				summad[i] = reaSumma(maatriks[i]);
			}
			return summad;
		}
		/*Eraldi meetod ühe rea summa arvutamiseks
		 */
		public static int reaSumma(int[] massiiv) {
			int summa = 0;
			for (int arv : massiiv) {
				summa += arv;
			}
			return summa;
		}
		
		
		
		/*Arvutada kõrvaldiagonaali elementide summa 
		 * (kõrvaldiagonaal on see, mis jookseb ülevalt paremast nurgast alla vasakusse nurka).
		 */
		public static int korvalDiagonaaliSumma(int[][] maatriks){
			int summa = 0;
			for (int i = 0; i < maatriks.length; i++) {
				summa += maatriks[i][maatriks[i].length - i-1];
			}
			return summa;
					
		}
		public static int peaDiagonaaliSumma(int[][] maatriks){
			int summa = 0;
			for (int i = 0; i < maatriks.length; i++) {
				for (int j = 0; j < maatriks[i].length; j++) {
					if ( i == j ){
						summa += maatriks[i][j];
					}
					//System.out.println(maatriks[i][j]);
				}
				//System.out.println();
			}
			return summa;		
		}
		public static int peaDiagonaaliSummaEf(int[][] maatriks){
			int summa = 0;
			for (int i = 0; i < maatriks.length; i++) {
				summa += maatriks[i][i];
			}
			return summa;
		}
		
		/*Leida iga rea suurim element.
		 * 
		 */
		public static int[] ridadeMaksimumid(int[][] maatriks){
			int[] maksimumid = new int[maatriks.length];
		 for (int i = 0; i < maatriks.length; i++) {
		        if (maatriks[i] > maatriks.length) {
		            maksimumid = maatriks[i];
		        }
		return maksimumid;
		}
		}
		
		
		public static int reaMaksimum(int[] massiiv){
			int maksimum = Integer.MIN_VALUE;
			for (int arv : massiiv) {
				if (arv > maksimum){
					maksimum = arv;
				}
			}
			return maksimum;
		}

		}
		

