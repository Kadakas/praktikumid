package praktikum9;

import lib.TextIO;

public class Laul {

	public static void main(String[] args) {
		String laul = Laulud.p6der;

		System.out.println("Sisesta täht, millega asendame:");
		char t2ht = TextIO.getChar();

		String uuslaul = "";

		for (int i = 0; i < laul.length(); i++) {

			if (t2ishKontroll(laul.charAt(i))) {

				uuslaul += t2ht;

			} else {
				uuslaul += laul.charAt(i);
			}

		}
		System.out.println(uuslaul);
	}

	static boolean t2ishKontroll(char t2ht) {

		char[] t2hed = { 'a', 'e', 'i', 'o', 'u', 'õ', 'ä', 'ö', 'ü' };

		int j = 0;
		boolean tagastus = false;

		while (j < t2hed.length) {

			if (t2ht == t2hed[j]) {
				tagastus = true;
				return tagastus;
			}
			j++;
		}

		return tagastus;

	}
}
